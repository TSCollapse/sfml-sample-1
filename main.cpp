﻿#include <SFML/Graphics.hpp>
#include <thread>
#include <chrono>

int main()
{
    sf::RenderWindow window(sf::VideoMode(800, 600), "Circle");
    sf::CircleShape shape(100.0);
    shape.setOrigin(100, 100);
    shape.setFillColor(sf::Color::Green);
    int shape_x = 100, shape_y = 0;
    shape.setPosition(shape_x, shape_y);

    
    sf::CircleShape shape2(150.0);
    shape2.setOrigin(150, 150);
    shape2.setFillColor(sf::Color::Red);
    int shape2_x = 400, shape2_y = 300;
    shape2.setPosition(shape2_x, shape2_y);

    sf::CircleShape shape3(50.0);
    shape3.setOrigin(50, 50);
    shape3.setFillColor(sf::Color::Blue);
    int shape3_x = 700, shape3_y = 600;
    shape3.setPosition(shape3_x, shape3_y);


    while (window.isOpen())
    {
        sf::Event event;
        while (window.pollEvent(event))
        {
            if (event.type == sf::Event::Closed)
                window.close();
        }
        shape_y++;
        if (shape_y > 600)
            shape_y = 600;
        shape.setPosition(shape_x, shape_y);

        shape2_y++;
        if (shape2_y > 600)
            shape2_y = 600;
        shape2.setPosition(shape2_x, shape2_y);

        shape3_y++;
        if (shape3_y > 600)
            shape3_y = 600;
        shape3.setPosition(shape3_x, shape3_y);


        window.clear();
        window.draw(shape);
        window.draw(shape2);
        window.draw(shape3);
        window.display();
    }

    return 0;
}